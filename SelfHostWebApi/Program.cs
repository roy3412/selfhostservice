﻿using ServiceInterface;
using System.Configuration;

namespace SelfHostWebApi
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new SelfHostService(ConfigurationManager.AppSettings["serviceEndpoint"]);
            var t = service.Start(new Payment());
            t.Wait();
        }
    }
}
