﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.SelfHost;
using Unity;
using Unity.WebApi;

namespace ServiceInterface
{
    public class SelfHostService
    {
        private readonly string _baseAddress;

        public SelfHostService(string baseAddress)
        {
            _baseAddress = baseAddress;
        }

        public async Task Start(IPayment payment)
        {
            var config = new HttpSelfHostConfiguration(_baseAddress);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "API Default", "api/{controller}/{action}/{id}",
                new { id = RouteParameter.Optional });

            var container = new UnityContainer();
            container.RegisterInstance(payment);
            config.DependencyResolver = new UnityDependencyResolver(container);

            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            {
                await server.OpenAsync();
            }
        }
    }
}
