﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace ServiceInterface
{
    [RoutePrefix("api/payment")]
    public class PaymentController : ApiController
    {
        private readonly IPayment _payment;

        public PaymentController(IPayment payment)
        {
            _payment = payment;
        }

        [HttpGet]
        [Route("deposit/{amount}")]
        public async Task<string> Deposit(int amount)
        {
            return _payment.Depoist(amount);
        }

        public async Task<int> Get()
        {
            return 1;
        } 
    }
}
